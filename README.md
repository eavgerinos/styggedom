# Styggedom

DISCLAIMER: This is just a Software Engineering assignment. It's not
fully functional nor with ever be. DO NOT USE IT for production. It is
not meant to be used by any company or individual who actually wants
a software as the one described below.

Styggedom is an HR tool.

## Installation Requirements

First you need to install [Java Runtime Environment](https://www.java.com/en/download/help/index_installing.xml)
following the instriuctions provided. The application is fully written
in Java, therefore you need the JVM to run it.

Then you need to install
[MySQL](http://dev.mysql.com/downloads/windows/) following the
instructions provided, in order to use it as a data storage.

After installing MySQL you must login to it with

```
Username: root
Password: root
```

and create the database like this

```SQL
CREATE DATABASE styggedom;
```

After starting the application there is a bootstrap method that will
create the database structure along with a default Admin user.

Though, if you want to get the full experience of the application out of
the box you need to import the `styggedom.sql` provided in the root path
of the source tree of this application.

In order to do so, after you have created the database as stated above
you need to execute the following command on your prefered command-line
tool

```shell
> mysql -u root -proot styggedom < styggedom.sql
```

The bootstraping procedure runs seemlessly and it is intented to hide
its precense from the end user.

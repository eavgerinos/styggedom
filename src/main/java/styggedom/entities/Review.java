package styggedom.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import styggedom.dao.ReviewDao;

@Entity
@Table(name = "reviews")
@NamedQueries({
		@NamedQuery(name = "Review.findAll", query = "SELECT r FROM Review r"),
		@NamedQuery(name = "Review.findByEmployee", query = "SELECT r FROM Review r WHERE employee = :employee"),
		@NamedQuery(name = "Review.findByEmployeeAndContent", query = "SELECT r FROM Review r WHERE employee = :employee AND content = :content") })
public class Review {

	public static ReviewDao reviewDao = new ReviewDao();

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@ManyToOne
	@JoinColumn(name = "employee_id")
	private User employee;

	@ManyToOne
	@JoinColumn(name = "reviewer_id")
	private User reviewer;

	@Column(name = "stars")
	private Integer stars;

	@Column(name = "content")
	private String content;

	public User getEmployee() {
		return employee;
	}

	public void setEmployee(User employee) {
		this.employee = employee;
	}

	public User getReviewer() {
		return reviewer;
	}

	public void setReviewer(User reviewer) {
		this.reviewer = reviewer;
	}

	public Integer getStars() {
		return stars;
	}

	public void setStars(Integer stars) {
		this.stars = stars;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Long getId() {
		return id;
	}

	public static int countAll() {
		return reviewDao.fetchAll().size();
	}
}

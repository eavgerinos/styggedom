package styggedom.entities;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Entity;

import styggedom.dao.UserDao;

@Entity
@Table(name = "users")
@NamedQueries({
		@NamedQuery(name = "User.findAll", query = "SELECT u FROM User u"),
		@NamedQuery(name = "User.findById", query = "SELECT u FROM User u WHERE u.id = :id"),
		@NamedQuery(name = "User.findByUsername", query = "SELECT u FROM User u WHERE u.username = :userName"),
		@NamedQuery(name = "User.findByFullName", query = "SELECT u FROM User u WHERE u.fullName = :fullName"),
		@NamedQuery(name = "User.findByEmail", query = "SELECT u FROM User u WHERE u.email = :email"),
		@NamedQuery(name = "User.findAdmins", query = "SELECT u FROM User u WHERE u.isAdmin = true"),
		@NamedQuery(name = "User.findByDepartment", query = "SELECT u FROM User u WHERE u.department = :department"),
		@NamedQuery(name = "User.countAll", query = "SELECT count(u) FROM User u"),
		@NamedQuery(name = "User.findByUsernameAndPassword", query = "SELECT u FROM User u WHERE u.username = :userName AND u.password = :password") })
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "full_name")
	private String fullName;

	@Column(name = "is_manager")
	private Boolean isManager;

	@Column(name = "is_admin")
	private Boolean isAdmin;

	@Column(name = "is_ceo")
	private Boolean isCEO;

	@Column(name = "username")
	private String username;

	@Column(name = "email")
	private String email;

	// Plain-text here. Don't want to add complexity to this dummy app.
	@Column(name = "password")
	private String password;

	@ManyToOne
	@JoinColumn(name = "department_id")
	private Department department;

	public static UserDao userDao = new UserDao();

	@Override
	public String toString() {
		return fullName;
	}

	public Long getId() {
		return id;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getFullName() {
		return this.toString();
	}

	public Boolean getIsManager() {
		return isManager;
	}

	public void setIsManager(Boolean flag) {
		this.isManager = flag;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public Boolean getIsAdmin() {
		return isAdmin;
	}

	public void setIsAdmin(Boolean flag) {
		this.isAdmin = flag;
	}

	public Boolean getIsCEO() {
		return isCEO;
	}

	public void setIsCEO(Boolean flag) {
		this.isCEO = flag;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public static int countAll() {
		return userDao.fetchAll().size();
	}

}

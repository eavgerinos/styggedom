package styggedom.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.EntityManagerFactory;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.NamedQueries;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Persistence;
import javax.persistence.Table;
import javax.persistence.Entity;

import styggedom.dao.DepartmentDao;

@Entity
@Table(name = "departments")
@NamedQueries({
		@NamedQuery(name = "Department.findAll", query = "SELECT d FROM Department d"),
		@NamedQuery(name = "Department.findById", query = "SELECT d FROM Department d WHERE d.id = :id"),
		@NamedQuery(name = "Department.findByName", query = "SELECT d FROM Department d WHERE d.name = :name"),
		@NamedQuery(name = "Department.findByManager", query = "SELECT d FROM Department d WHERE d.manager = :manager") })
public class Department {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "name")
	private String name;

	@OneToOne
	@JoinColumn(name = "manager_id")
	private User manager;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<User> getUsers() {
		return users;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}
	
	public static int countAll() {
		return departmentDao.fetchAll().size();
	}

	@OneToMany
	@JoinColumn(name = "department_id")
	private Set<User> users = new HashSet<User>();

	public static DepartmentDao departmentDao = new DepartmentDao();

	private static EntityManagerFactory emf = Persistence
			.createEntityManagerFactory("styggedom.jpa");

	public static EntityManagerFactory getEmf() {
		return emf;
	}

	@Override
	public String toString() {
		return name;
	}

	public User getManager() {
		return manager;
	}

	public void setManager(User manager) {
		this.manager = manager;
		manager.setIsManager(true);
	}

	public void addUser(User user) {
		user.setDepartment(this);
		users.add(user);
	}
}

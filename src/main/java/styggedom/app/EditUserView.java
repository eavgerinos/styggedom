package styggedom.app;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.List;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;

import javax.swing.JLabel;
import javax.swing.JTextField;

import styggedom.entities.Department;
import styggedom.entities.User;

import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class EditUserView extends JFrame {
	private static JTextField fullNameTextField;
	private static JTextField usernameTextField;
	private static JTextField passwordTextField;
	private static JTextField emailTextField;
	private static JCheckBox isAdminCheckBox;
	private static JCheckBox isCEOCheckBox;
	private static JCheckBox isManagerCheckBox;
	private static JComboBox<Department> departmentsComboBox;
	private JButton btnUpdate;

	/**
	 * Create the frame.
	 */
	public EditUserView(final User user) {
		setBounds(100, 100, 658, 425);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation(dim.width / 2 - this.getSize().width / 2, dim.height
				/ 2 - this.getSize().height / 2);
		setResizable(false);
		getContentPane().setLayout(null);

		JLabel lblFullName = new JLabel("Full Name");
		lblFullName.setBounds(35, 26, 84, 16);
		getContentPane().add(lblFullName);

		fullNameTextField = new JTextField();
		fullNameTextField.setBounds(131, 20, 134, 28);
		getContentPane().add(fullNameTextField);
		fullNameTextField.setColumns(10);
		fullNameTextField.setText(user.getFullName());

		JLabel lblUsername = new JLabel("Username");
		lblUsername.setBounds(35, 54, 84, 16);
		getContentPane().add(lblUsername);

		usernameTextField = new JTextField();
		usernameTextField.setBounds(131, 48, 134, 28);
		getContentPane().add(usernameTextField);
		usernameTextField.setColumns(10);
		usernameTextField.setText(user.getUsername());

		JLabel lblPassword = new JLabel("Password");
		lblPassword.setBounds(35, 82, 84, 16);
		getContentPane().add(lblPassword);

		passwordTextField = new JTextField();
		passwordTextField.setBounds(131, 76, 134, 28);
		getContentPane().add(passwordTextField);
		passwordTextField.setColumns(10);
		passwordTextField.setText(user.getPassword());

		JLabel lblEmail = new JLabel("E-mail");
		lblEmail.setBounds(35, 110, 84, 16);
		getContentPane().add(lblEmail);

		emailTextField = new JTextField();
		emailTextField.setBounds(131, 104, 134, 28);
		getContentPane().add(emailTextField);
		emailTextField.setColumns(10);
		emailTextField.setText(user.getEmail());

		JLabel lblManager = new JLabel("Manager");
		lblManager.setBounds(35, 138, 61, 16);
		getContentPane().add(lblManager);

		isManagerCheckBox = new JCheckBox("");
		isManagerCheckBox.setBounds(131, 134, 128, 23);
		getContentPane().add(isManagerCheckBox);
		isManagerCheckBox.setSelected(user.getIsManager());

		JLabel lblCeo = new JLabel("CEO");
		lblCeo.setBounds(35, 166, 84, 16);
		getContentPane().add(lblCeo);

		isCEOCheckBox = new JCheckBox("");
		isCEOCheckBox.setBounds(131, 162, 128, 23);
		getContentPane().add(isCEOCheckBox);
		isCEOCheckBox.setSelected(false);

		List<Department> departments = Department.departmentDao.fetchAll();
		departmentsComboBox = new JComboBox<Department>();
		departmentsComboBox.setBounds(131, 225, 52, 27);
		for (Department d : departments) {
			departmentsComboBox.addItem(d);
		}

		getContentPane().add(departmentsComboBox);

		JLabel lblNewLabel = new JLabel("Admin");
		lblNewLabel.setBounds(35, 194, 84, 16);
		getContentPane().add(lblNewLabel);

		isAdminCheckBox = new JCheckBox("");
		isAdminCheckBox.setBounds(131, 190, 128, 23);
		getContentPane().add(isAdminCheckBox);
		isAdminCheckBox.setSelected(user.getIsAdmin());

		btnUpdate = new JButton("Update");
		btnUpdate.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				updateUser(user);
			}
		});

		btnUpdate.setBounds(131, 264, 117, 29);
		getContentPane().add(btnUpdate);

	}

	private static void updateUser(User user) {
		User.userDao.update(user, fullNameTextField.getText(),
				usernameTextField.getText(), emailTextField.getText(),
				passwordTextField.getText(), isCEOCheckBox.isSelected(),
				isAdminCheckBox.isSelected(), isManagerCheckBox.isSelected(),
				(Department) departmentsComboBox.getSelectedItem());
	}
}

package styggedom.app;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import styggedom.entities.Review;
import styggedom.entities.User;

@SuppressWarnings("serial")
public class ShowUserReviews extends JFrame {

	private JPanel contentPane;
	private DefaultTableModel reviewsModel;
	private JTable reviewsTable;
	private User employee;

	/**
	 * Create the frame.
	 */
	public ShowUserReviews(User user) {
		this.employee = user;
		this.setTitle("Reviews for " + user.getFullName());
		setBounds(100, 100, 870, 487);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation(dim.width / 2 - this.getSize().width / 2, dim.height
				/ 2 - this.getSize().height / 2);
		setResizable(false);
		setContentPane(contentPane);

		String[] columnNames = new String[] { "Rating", "Content", "Reviewer" };
		reviewsModel = new DefaultTableModel(null, columnNames);
		getContentPane().setLayout(null);
		reviewsTable = new JTable(reviewsModel);
		reviewsTable.setSurrendersFocusOnKeystroke(true);
		reviewsTable.setBounds(0, 333, 506, -333);
		JScrollPane scrollPane = new JScrollPane(reviewsTable);
		scrollPane.setBounds(6, 39, 858, 420);
		reviewsTable.setRowSelectionAllowed(true);
		reviewsTable.setColumnSelectionAllowed(false);
		reviewsTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		refreshReviewsTable(reviewsModel, user);
		contentPane.setLayout(null);
		contentPane.add(scrollPane);

		JButton btnRefreshTable = new JButton("Refresh Table");
		btnRefreshTable.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				refreshReviewsTable(reviewsModel, employee);
			}
		});
		btnRefreshTable.setBounds(6, 6, 117, 29);
		contentPane.add(btnRefreshTable);

		JButton btnDestroyReview = new JButton("Destroy Review");
		btnDestroyReview.setBounds(135, 6, 147, 29);
		contentPane.add(btnDestroyReview);

		btnDestroyReview.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				int selectedRow = reviewsTable.getSelectedRow();

				if (selectedRow > -1) {
					String content = (String) reviewsTable.getValueAt(
							selectedRow, 1);
					Review review = Review.reviewDao.findByEmployeeAndContent(
							employee, content);

					Review.reviewDao.destroy(review);
					
					refreshReviewsTable(reviewsModel, employee);
				}
			}
		});
	}

	public static void refreshReviewsTable(DefaultTableModel reviewsModel,
			User employee) {
		reviewsModel.setRowCount(0);

		List<Review> reviews = Review.reviewDao.findByEmployee(employee);

		for (Review r : reviews) {
			Vector<String> reviewVector = new Vector<String>();
			reviewVector.add(r.getStars().toString());
			reviewVector.add(r.getContent());
			reviewVector.add(r.getReviewer().getFullName());

			reviewsModel.addRow(reviewVector);
		}
	}
}

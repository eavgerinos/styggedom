package styggedom.app;

import java.util.List;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JButton;

import styggedom.entities.Department;
import styggedom.entities.User;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class NewUser extends JPanel {
	private JTextField fullNameTextField;
	private JTextField usernameTextField;
	private JTextField passwordTextField;
	private JTextField emailTextField;
	private JCheckBox isAdminCheckBox;
	private JCheckBox isCEOCheckBox;
	private JCheckBox isManagerCheckBox;
	private JComboBox<Department> departmentsComboBox;
	UsersListModel ulModel;

	/**
	 * Create the panel.
	 */
	public NewUser(final UsersListModel ulModel) {
		setLayout(null);
		this.ulModel = ulModel;

		JLabel lblFullName = new JLabel("Full Name");
		lblFullName.setBounds(35, 26, 84, 16);
		add(lblFullName);

		fullNameTextField = new JTextField();
		fullNameTextField.setBounds(131, 20, 134, 28);
		add(fullNameTextField);
		fullNameTextField.setColumns(10);

		JLabel lblUsername = new JLabel("Username");
		lblUsername.setBounds(35, 54, 84, 16);
		add(lblUsername);

		usernameTextField = new JTextField();
		usernameTextField.setBounds(131, 48, 134, 28);
		add(usernameTextField);
		usernameTextField.setColumns(10);

		JLabel lblPassword = new JLabel("Password");
		lblPassword.setBounds(35, 82, 84, 16);
		add(lblPassword);

		passwordTextField = new JTextField();
		passwordTextField.setBounds(131, 76, 134, 28);
		add(passwordTextField);
		passwordTextField.setColumns(10);

		JLabel lblEmail = new JLabel("E-mail");
		lblEmail.setBounds(35, 110, 84, 16);
		add(lblEmail);

		emailTextField = new JTextField();
		emailTextField.setBounds(131, 104, 134, 28);
		add(emailTextField);
		emailTextField.setColumns(10);

		JLabel lblManager = new JLabel("Manager");
		lblManager.setBounds(35, 138, 61, 16);
		add(lblManager);

		isManagerCheckBox = new JCheckBox("");
		isManagerCheckBox.setBounds(131, 134, 128, 23);
		add(isManagerCheckBox);

		JLabel lblCeo = new JLabel("CEO");
		lblCeo.setBounds(35, 166, 84, 16);
		add(lblCeo);

		isCEOCheckBox = new JCheckBox("");
		isCEOCheckBox.setBounds(131, 162, 128, 23);
		add(isCEOCheckBox);

		List<Department> departments = Department.departmentDao.fetchAll();

		departmentsComboBox = new JComboBox<Department>();
		departmentsComboBox.setBounds(131, 225, 52, 27);
		for (Department d : departments) {
			departmentsComboBox.addItem(d);
		}

		add(departmentsComboBox);

		JLabel lblNewLabel = new JLabel("Admin");
		lblNewLabel.setBounds(35, 194, 84, 16);
		add(lblNewLabel);

		isAdminCheckBox = new JCheckBox("");
		isAdminCheckBox.setBounds(131, 190, 128, 23);
		add(isAdminCheckBox);

		JButton btnCreate = new JButton("Create");
		btnCreate.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				User user = new User();
				user.setDepartment((Department) departmentsComboBox
						.getSelectedItem());
				user.setFullName(fullNameTextField.getText());
				user.setUsername(usernameTextField.getText());
				user.setEmail(emailTextField.getText());
				user.setIsAdmin(isAdminCheckBox.isSelected());
				user.setIsCEO(isCEOCheckBox.isSelected());
				user.setPassword(passwordTextField.getText());
				user.setIsManager(isManagerCheckBox.isSelected());
				
				User.userDao.persist(user);

				departmentsComboBox.setSelectedItem(null);
				fullNameTextField.setText("");
				usernameTextField.setText("");
				emailTextField.setText("");
				passwordTextField.setText("");
				isAdminCheckBox.setSelected(false);
				isCEOCheckBox.setSelected(false);
				isManagerCheckBox.setSelected(false);
				
				ulModel.notifyObservers();
			}
		});
		btnCreate.setBounds(131, 265, 117, 29);
		add(btnCreate);

		JLabel lblDepartment = new JLabel("Department");
		lblDepartment.setBounds(35, 226, 84, 16);
		add(lblDepartment);
	}
}

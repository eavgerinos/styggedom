package styggedom.app;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JPanel;

import styggedom.entities.User;
import styggedom.utils.DatabaseBootstrapper;

import javax.swing.JPasswordField;

public class LoginPage {

	private JFrame frmHrManagement;
	private MainPageView mainPage;
	private JTextField usernameTextField;
	private JPanel loginPanel;
	private JPasswordField passwordField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		// Bootstrap Database
		DatabaseBootstrapper bs = new DatabaseBootstrapper();
		bs.bootstrap();

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
                    LoginPage window = new LoginPage();
					window.frmHrManagement.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public LoginPage() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmHrManagement = new JFrame();
		frmHrManagement.setResizable(false);
		frmHrManagement.setTitle("HR Management");
		frmHrManagement.setBounds(100, 100, 762, 568);
		frmHrManagement.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmHrManagement.getContentPane().setLayout(null);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		frmHrManagement.setLocation(dim.width/2-frmHrManagement.getSize().width/2, dim.height/2-frmHrManagement.getSize().height/2);

		loginPanel = new JPanel();
		loginPanel.setBounds(0, 6, 756, 540);
		frmHrManagement.getContentPane().add(loginPanel);
		loginPanel.setLayout(null);

		usernameTextField = new JTextField();
		usernameTextField.setBounds(366, 232, 134, 28);
		loginPanel.add(usernameTextField);
		usernameTextField.setColumns(10);

		JLabel lblPassword = new JLabel("Password");
		lblPassword.setBounds(286, 272, 61, 16);
		loginPanel.add(lblPassword);

		JButton btnLogin = new JButton("Login");
		btnLogin.setBounds(312, 306, 117, 29);
		loginPanel.add(btnLogin);

		JLabel lblUsername = new JLabel("Username");
		lblUsername.setBounds(280, 238, 71, 16);
		loginPanel.add(lblUsername);

		passwordField = new JPasswordField();
		passwordField.setBounds(366, 266, 134, 28);
		loginPanel.add(passwordField);
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				String username = usernameTextField.getText();
				@SuppressWarnings("deprecation")
				String password = passwordField.getText();

				User user = User.userDao.authenticate(username, password);

				if (user == null) {
					JOptionPane.showMessageDialog(null,
							"You username/password combination is wrong. "
									+ "Please try again");
				} else {
					frmHrManagement.setVisible(false);
					frmHrManagement.dispose();

					mainPage = new MainPageView(user);
				}
			}
		});
	}
}

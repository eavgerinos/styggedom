package styggedom.app;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;

import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;

import styggedom.entities.Review;
import styggedom.entities.User;

public class ReviewUserController implements ActionListener {

	private User userForReview, reviewer;
	private JFormattedTextField reviewTextField;
	private JComboBox<Integer> ratingComboBox;

	public ReviewUserController(User userForReview, User reviewer,
			JComboBox<Integer> ratingComboBox,
			JFormattedTextField reviewTextField) {
		this.reviewer = reviewer;
		this.userForReview = userForReview;
		this.reviewTextField = reviewTextField;
		this.ratingComboBox = ratingComboBox;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Review review = new Review();
		review.setReviewer(reviewer);
		review.setEmployee(userForReview);

		try {
			reviewTextField.commitEdit();
		} catch (ParseException e1) {
			e1.printStackTrace();
		}

		review.setContent(reviewTextField.getText());
		review.setStars((int) ratingComboBox.getSelectedItem());

		Review.reviewDao.persist(review);

		reviewTextField.setText("");
	}
}

package styggedom.app;

import java.util.List;
import java.util.Observable;

import styggedom.entities.User;

public class UsersListModel extends Observable {
	private List<User> allUsers = User.userDao.fetchAll();
	
	public List<User> getAllUsers() {
		return allUsers;
	}
	
	public void refreshUsersList() {
		this.allUsers = User.userDao.fetchAll();
		setChanged();
		notifyObservers(allUsers);
	}
}

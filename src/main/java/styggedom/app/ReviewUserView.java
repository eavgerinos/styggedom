package styggedom.app;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import styggedom.entities.User;

@SuppressWarnings("serial")
public class ReviewUserView extends JFrame {

	private JPanel contentPane;

	/**
	 * Create the frame.
	 */
	public ReviewUserView(User userForReview, User reviewer) {
		setTitle("New Review");
		setBounds(100, 100, 654, 443);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation(dim.width / 2 - this.getSize().width / 2, dim.height
				/ 2 - this.getSize().height / 2);
		setResizable(false);
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JFormattedTextField contetTextField = new JFormattedTextField();
		contetTextField.setBounds(79, 137, 569, 138);
		contentPane.add(contetTextField);

		JLabel lblNewLabel = new JLabel("Content");
		lblNewLabel.setBounds(6, 198, 61, 16);
		contentPane.add(lblNewLabel);

		JComboBox<Integer> ratingComboBox = new JComboBox<Integer>();
		ratingComboBox.setBounds(79, 88, 200, 50);
		contentPane.add(ratingComboBox);

		int[] ratings = { 1, 2, 3, 4, 5 };

		for (int i : ratings) {
			ratingComboBox.addItem(i);
		}

		JLabel lblRating = new JLabel("Rating");
		lblRating.setBounds(6, 87, 200, 50);
		contentPane.add(lblRating);

		JLabel lblReview = new JLabel("Review for user "
				+ userForReview.getFullName());
		lblReview.setBounds(210, 25, 255, 16);
		contentPane.add(lblReview);

		JButton btnCreateReview = new JButton("Save");

		btnCreateReview.addActionListener(new ReviewUserController(
				userForReview, reviewer, ratingComboBox, contetTextField));
		btnCreateReview.setBounds(233, 328, 200, 50);
		contentPane.add(btnCreateReview);
	}
}

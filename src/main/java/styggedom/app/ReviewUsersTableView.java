package styggedom.app;

import java.awt.Dimension;
import java.util.List;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import styggedom.entities.User;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class ReviewUsersTableView extends JPanel {

	private JTable usersTable;
	private static User user;
	private JButton btnReviewUser, btnShowReviewsFor;
	private DefaultTableModel usersModel;
	private User reviewer;

	/**
	 * Create the frame.
	 */
	public ReviewUsersTableView(User user) {
		this.reviewer = user;
		ReviewUsersTableView.user = user;
		setBounds(100, 100, 828, 470);
		this.setBorder(new EmptyBorder(5, 5, 5, 5));
		String[] column_names = new String[] { "Name", "Username", "E-mail",
				"Manager", "Admin", "Department" };

		usersModel = new DefaultTableModel(null, column_names);
		this.setLayout(null);
		setLayout(null);
		usersTable = new JTable(usersModel);

		usersTable.setPreferredSize(new Dimension(400, 400));
		JScrollPane scrollPane = new JScrollPane(usersTable);
		scrollPane.setBounds(6, 44, 816, 420);
		usersTable.setRowSelectionAllowed(true);
		usersTable.setColumnSelectionAllowed(false);
		usersTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		this.add(scrollPane);

		JButton btnRefreshTable = new JButton("Refresh Table");
		btnRefreshTable.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				refreshUsersTable(usersModel);
			}
		});
		btnRefreshTable.setBounds(6, 6, 117, 29);
		this.add(btnRefreshTable);

		btnReviewUser = new JButton("Review User");
		btnReviewUser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int selectedRow = usersTable.getSelectedRow();
				if (selectedRow > -1) {
					String username = (String) usersTable.getValueAt(
							selectedRow, 1);
					User selectedUser = User.userDao.findByUsername(username);
					ReviewUserView ruv = new ReviewUserView(selectedUser,
							reviewer);
					
					ruv.setVisible(true);
				}
			}
		});
		btnReviewUser.setEnabled(false);
		btnReviewUser.setBounds(135, 6, 117, 29);
		this.add(btnReviewUser);
		
		btnShowReviewsFor = new JButton("Show Reviews For User");
		btnShowReviewsFor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int selectedRow = usersTable.getSelectedRow();
				if (selectedRow > -1) {
					String username = (String) usersTable.getValueAt(selectedRow, 1);
					User selectedUser = User.userDao.findByUsername(username);
					ShowUserReviews sur = new ShowUserReviews(selectedUser);
					sur.setVisible(true);
				}
			}
		});
		btnShowReviewsFor.setEnabled(false);
		btnShowReviewsFor.setBounds(264, 3, 172, 29);
		add(btnShowReviewsFor);

		refreshUsersTable(usersModel);

		usersTable.getSelectionModel().addListSelectionListener(
				new ListSelectionListener() {

					@Override
					public void valueChanged(ListSelectionEvent e) {
						btnReviewUser.setEnabled(true);
						btnShowReviewsFor.setEnabled(true);
					}
				});
	}

	public static void refreshUsersTable(DefaultTableModel usersModel) {
		usersModel.setRowCount(0);
		List<User> users = null;

		if (user.getIsAdmin() || user.getIsCEO()
				|| user.getDepartment().getName().equals("HR")) {
			users = User.userDao.fetchAll();
		} else {
			users = User.userDao.findByDepartment(user.getDepartment());
		}

		for (User u : users) {
			Vector<String> userVector = new Vector<String>();
			userVector.add(u.getFullName());
			userVector.add(u.getUsername());
			userVector.add(u.getEmail());
			userVector.add(u.getIsManager().toString());
			userVector.add(u.getIsAdmin().toString());
			userVector.add(u.getDepartment().getName());

			usersModel.addRow(userVector);
		}
	}
}

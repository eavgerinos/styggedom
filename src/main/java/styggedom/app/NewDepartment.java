package styggedom.app;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JComboBox;

import styggedom.entities.Department;
import styggedom.entities.User;

import javax.swing.JButton;
import javax.swing.table.DefaultTableModel;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class NewDepartment extends JPanel {
	private JTextField deptNameTextField;
	private JComboBox<User> usersComboBox;

	/**
	 * Create the panel.
	 */
	public NewDepartment(final DefaultTableModel usersModel) {
		setLayout(null);

		JLabel lblName = new JLabel("Name");
		lblName.setBounds(17, 23, 61, 16);
		add(lblName);

		deptNameTextField = new JTextField();
		deptNameTextField.setBounds(90, 17, 134, 28);
		add(deptNameTextField);
		deptNameTextField.setColumns(10);

		JLabel lblManager = new JLabel("Manager");
		lblManager.setBounds(17, 51, 61, 16);
		add(lblManager);

		List<User> users = User.userDao.fetchAll();

		usersComboBox = new JComboBox<User>();
		usersComboBox.setBounds(90, 35, 200, 50);
		for (User u : users) {
			usersComboBox.addItem(u);
		}
		add(usersComboBox);

		JButton btnCreateDept = new JButton("Create");
		btnCreateDept.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Department department = new Department();
				department.setName(deptNameTextField.getText());
				department.setManager((User) usersComboBox.getSelectedItem());

				Department.departmentDao.persist(department);

				MainPageView.refreshUsersTable(usersModel);

				deptNameTextField.setText("");
				usersComboBox.setSelectedItem(null);
			}
		});
		btnCreateDept.setBounds(90, 97, 117, 29);
		add(btnCreateDept);
	}
}

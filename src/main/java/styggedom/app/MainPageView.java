package styggedom.app;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.JTabbedPane;

import styggedom.entities.User;

import javax.swing.JTable;
import javax.swing.JButton;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

@SuppressWarnings("serial")
public class MainPageView extends JFrame implements Observer {

	private JPanel contentPane;
	private JTable usersTable;
	private User currentUser;
	public JTabbedPane tabbedPane;
	JButton btnRefreshTable;
	JButton btnDeleteUser;
	JButton btnEditUser;
	DefaultTableModel usersModel;

	/**
	 * Create the frame.
	 */
	public MainPageView(User user) {
		currentUser = user;
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1037, 506);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);

		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(6, 6, 1024, 472);
		contentPane.add(tabbedPane);

		// if (user.getIsAdmin() || user.getIsCEO()) {
		// Users Management Panel for Admins
		JPanel usersPanel = new JPanel();
		tabbedPane.addTab("Users", null, usersPanel, null);
		String[] column_names = new String[] { "Name", "Username", "Password",
				"E-mail", "Manager", "Admin", "Department" };
		usersModel = new DefaultTableModel(null, column_names);
		usersTable = new JTable(usersModel);
		// refreshUsersTable(usersModel);
		usersPanel.setLayout(null);
		usersTable.setPreferredSize(new Dimension(400, 400));
		JScrollPane scrollPane = new JScrollPane(usersTable);
		scrollPane.setBounds(6, 44, 991, 381);
		usersTable.setRowSelectionAllowed(true);
		usersTable.setColumnSelectionAllowed(false);
		usersTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		usersPanel.add(scrollPane);

		// MVC button
		btnRefreshTable = new JButton("Refresh Table");

		UsersListModel ulModel = new UsersListModel();
		UsersListController ulController = new UsersListController();

		ulModel.addObserver(this);
		ulController.addModel(ulModel);
		ulController.addView(this);
		this.addController(ulController);
		ulController.initModel();

		btnRefreshTable.setBounds(0, 3, 117, 29);
		usersPanel.add(btnRefreshTable);

		btnDeleteUser = new JButton("Delete User");
		btnDeleteUser.setEnabled(false);
		btnDeleteUser.setBounds(129, 3, 117, 29);
		usersPanel.add(btnDeleteUser);

		btnEditUser = new JButton("Edit User");
		btnEditUser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int selectedRow = usersTable.getSelectedRow();
				if (selectedRow > -1) {
					String username = (String) usersTable.getValueAt(selectedRow, 1);
					User selectedUser = User.userDao.findByUsername(username);
					EditUserView euv = new EditUserView(selectedUser);
					euv.setVisible(true);
					
				}
			}
		});
		btnEditUser.setEnabled(false);
		btnEditUser.setBounds(258, 3, 117, 29);
		usersPanel.add(btnEditUser);

		btnDeleteUser.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				int selectedRow = usersTable.getSelectedRow();
				if (selectedRow > -1) {
					String username = (String) usersTable.getValueAt(selectedRow, 1);
					User selectedUser = User.userDao.findByUsername(username);
					User.userDao.destroy(selectedUser);
					refreshUsersTable(usersModel);
				}
			}
		});

		// Action buttons enabling upon user selection
		usersTable.getSelectionModel().addListSelectionListener(
				new ListSelectionListener() {
					@Override
					public void valueChanged(ListSelectionEvent e) {
						btnEditUser.setEnabled(true);
						btnDeleteUser.setEnabled(true);
					}
				});

		usersTable.setVisible(true);

		JPanel createUserPanel = new NewUser(ulModel);
		tabbedPane.addTab("New User", null, createUserPanel, null);

		JPanel createDepartmentPanel = new NewDepartment(usersModel);
		tabbedPane.addTab("New Department", null, createDepartmentPanel, null);
		
		JPanel reviewUserPanel = new ReviewUsersTableView(currentUser);
		tabbedPane.addTab("Review Users Table", null, reviewUserPanel, null);
		setVisible(true);
		// }
	}

	public static void refreshUsersTable(DefaultTableModel usersModel) {
		usersModel.setRowCount(0);

		List<User> allUsers = User.userDao.fetchAll();

		for (User u : allUsers) {
			Vector<String> userVector = new Vector<String>();
			userVector.add(u.getFullName());
			userVector.add(u.getUsername());
			userVector.add(u.getPassword());
			userVector.add(u.getEmail());
			userVector.add(u.getIsManager().toString());
			userVector.add(u.getIsAdmin().toString());
			userVector.add(u.getDepartment().getName());

			usersModel.addRow(userVector);
		}
	}

	public void addController(ActionListener controller) {
		btnRefreshTable.addActionListener(controller);
	}

	@Override
	public void update(Observable o, Object arg) {
		refreshUsersTable((DefaultTableModel) usersTable.getModel());
	}
}

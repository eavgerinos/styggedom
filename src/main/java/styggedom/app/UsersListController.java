package styggedom.app;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class UsersListController implements ActionListener {
	
	UsersListModel model;
	MainPageView view;

	@Override
	public void actionPerformed(ActionEvent e) {
		model.refreshUsersList();
	}
	
	public void addView(MainPageView view) {
		this.view = view;
	}
	
	public void addModel(UsersListModel model) {
		this.model = model;
	}
	
	public void initModel() {
		model.refreshUsersList();
	}
}

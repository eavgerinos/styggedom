package styggedom.utils;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;

import styggedom.entities.Department;
import styggedom.entities.User;

public class DatabaseBootstrapper {
	private static User admin;

	public DatabaseBootstrapper() {
		admin = null;
	}

	public void bootstrap() {

		if (needsAdminUser()) {
			setupAdminUser();
		}

		if (needsItDepartment()) {
			setupItDepartment(admin);
		}
	}

	public static void setupAdminUser() {

		User user = new User();
		user.setUsername("admin");
		user.setPassword("admin");
		user.setFullName("Administrator");
		user.setIsAdmin(true);
		user.setIsManager(true);

		User.userDao.persist(user);

		admin = user;
	}

	public static void setupItDepartment(User itManager) {
		EntityManager entityManager = Department.getEmf().createEntityManager();

		Department it = new Department();
		it.setManager(itManager);
		it.setName("IT");

		entityManager.getTransaction().begin();
		entityManager.persist(it);
		entityManager.getTransaction().commit();

		entityManager.detach(it);

		it.addUser(itManager);

		entityManager.getTransaction().begin();
		entityManager.merge(it);
		entityManager.getTransaction().commit();

		entityManager.close();
		

		itManager.setDepartment(it);
		entityManager.getTransaction().begin();
		entityManager.merge(itManager);
		entityManager.getTransaction().commit();

		entityManager.close();
		entityManager = null;
	}

	public static Boolean needsAdminUser() {
		List<User> list = User.userDao.findAdmins();
		
		if (!list.isEmpty()) {
			admin = list.get(0);
		}

		return list.isEmpty();
	}

	public static boolean needsItDepartment() {
		EntityManager entityManager = Department.getEmf().createEntityManager();

		TypedQuery<Department> query = entityManager.createNamedQuery(
				"Department.findByName", Department.class).setParameter("name",
				"IT");

		List<Department> list = query.getResultList();

		return list.isEmpty();
	}

}

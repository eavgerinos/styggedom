package styggedom.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import styggedom.entities.Review;
import styggedom.entities.User;

public class ReviewDao implements DaoInterface<Review, Long> {

	protected EntityManager entityManager;

	private static EntityManagerFactory emf = Persistence
			.createEntityManagerFactory("styggedom.jpa");

	public ReviewDao() {
		entityManager = emf.createEntityManager();
	}

	@Override
	public void persist(Review entity) {
		EntityTransaction tx = entityManager.getTransaction();
		tx.begin();
		entityManager.persist(entity);
		tx.commit();
	}

	@Override
	public void destroy(Review entity) {
		EntityTransaction tx = entityManager.getTransaction();
		tx.begin();
		entityManager.remove(entityManager.contains(entity) ? entity
				: entityManager.merge(entity));
		tx.commit();
	}

	@Override
	public Review fetch(Long uid) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Review> fetchAll() {
		TypedQuery<Review> query = (TypedQuery<Review>) entityManager
				.createNamedQuery("Review.findAll");

		List<Review> reviews = (List<Review>) query.getResultList();

		return reviews;
	}

	public List<Review> findByEmployee(User employee) {
		TypedQuery<Review> query = (TypedQuery<Review>) entityManager
				.createNamedQuery("Review.findByEmployee", Review.class)
				.setParameter("employee", employee);

		List<Review> reviews = (List<Review>) query.getResultList();

		return reviews;
	}

	public Review findByEmployeeAndContent(User employee, String content) {
		TypedQuery<Review> query = entityManager
				.createNamedQuery("Review.findByEmployeeAndContent", Review.class)
				.setParameter("employee", employee)
				.setParameter("content", content);
		
		List<Review> reviews = (List<Review>) query.getResultList();
		
		return reviews.get(0);
	}
}

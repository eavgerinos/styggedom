package styggedom.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import styggedom.entities.Department;

public class DepartmentDao implements DaoInterface<Department, Long> {
	@PersistenceContext
	protected EntityManager entityManager;

	private static EntityManagerFactory emf = Persistence
			.createEntityManagerFactory("styggedom.jpa");

	public DepartmentDao() {
		entityManager = emf.createEntityManager();
	}

	@Override
	public void persist(Department entity) {
		EntityTransaction tx = entityManager.getTransaction();
		tx.begin();
		entityManager.persist(entity);
		tx.commit();
	}

	@Override
	public void destroy(Department entity) {
		// Remove any user first
		if (!entity.getUsers().isEmpty()) {
			return;
		}

		EntityTransaction tx = entityManager.getTransaction();
		tx.begin();
		entityManager.remove(entityManager.contains(entity) ? entity
				: entityManager.merge(entity));
		tx.commit();
	}

	@Override
	public Department fetch(Long uid) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Department> fetchAll() {
		Query query = entityManager.createNamedQuery("Department.findAll",
				Department.class);

		List<Department> departments = (List<Department>) query.getResultList();

		return departments;
	}

}

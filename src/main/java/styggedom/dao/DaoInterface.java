package styggedom.dao;

import java.util.List;

public interface DaoInterface<E, K> {
	public void persist(E entity);
	
	public void destroy(E entity);
	
	public E fetch(K uid);
	
	public List<E> fetchAll();
}

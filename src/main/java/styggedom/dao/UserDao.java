package styggedom.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import styggedom.entities.Department;
import styggedom.entities.User;

public class UserDao implements DaoInterface<User, Long> {

	@PersistenceContext
	protected EntityManager entityManager;

	private static EntityManagerFactory emf = Persistence
			.createEntityManagerFactory("styggedom.jpa");

	public UserDao() {
		entityManager = emf.createEntityManager();
	}

	@Override
	public void persist(User entity) {
		EntityTransaction tx = entityManager.getTransaction();
		tx.begin();
		entityManager.persist(entity);
		tx.commit();
	}

	@Override
	public void destroy(User entity) {
		// Remove any managerial roles first
		if (entity.getIsManager() || entity.getIsAdmin() || entity.getIsCEO()) {
			return;
		}

		EntityTransaction tx = entityManager.getTransaction();
		tx.begin();
		entityManager.remove(entityManager.contains(entity) ? entity
				: entityManager.merge(entity));
		tx.commit();
	}

	@Override
	public User fetch(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<User> fetchAll() {
		Query query = entityManager
				.createNamedQuery("User.findAll", User.class);

		List<User> users = (List<User>) query.getResultList();

		return users;
	}

	public User authenticate(String username, String password) {
		TypedQuery<User> query = entityManager
				.createNamedQuery("User.findByUsernameAndPassword", User.class)
				.setParameter("userName", username)
				.setParameter("password", password);

		List<User> results = query.getResultList();

		if (results.size() != 1)
			return null;
		else
			return results.get(0);

	}

	public User findByUsername(String username) {
		TypedQuery<User> query = entityManager.createNamedQuery(
				"User.findByUsername", User.class).setParameter("userName",
				username);

		List<User> results = query.getResultList();

		if (results.size() != 1)
			return null;
		else
			return results.get(0);
	}

	public List<User> findAdmins() {
		TypedQuery<User> query = entityManager.createNamedQuery(
				"User.findAdmins", User.class);

		List<User> admins = (List<User>) query.getResultList();

		return admins;
	}

	public List<User> findByDepartment(Department department) {
		TypedQuery<User> query = entityManager.createNamedQuery(
				"User.findByDepartment", User.class).setParameter("department",
				department);

		List<User> users = (List<User>) query.getResultList();

		return users;
	}

	public void update(User user, String fullname, String username,
			String email, String password, Boolean isCEO, Boolean isAdmin,
			Boolean isManager, Department department) {

		EntityTransaction tx = entityManager.getTransaction();

		tx.begin();

		entityManager.flush();

		user.setUsername(username);
		user.setEmail(email);
		user.setDepartment(department);
		user.setFullName(fullname);
		user.setPassword(password);
		user.setIsAdmin(isAdmin);
		user.setIsCEO(isCEO);
		user.setIsManager(isManager);
		entityManager.merge(user);

		tx.commit();
	}
}

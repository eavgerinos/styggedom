package styggedom;

import static org.junit.Assert.*;

import org.junit.Test;

import styggedom.entities.User;
import styggedom.utils.DatabaseBootstrapper;

public class TestDatabaseBootstrapper {
	
	// If those two tests fail then it means you need to import the 
	// giben mysql dump.

	@Test
	public void itCreatesTheAdminUser() {
		DatabaseBootstrapper bs = new DatabaseBootstrapper();
		
		bs.bootstrap();
		
		User admin = User.userDao.findByUsername("admin");
		
		assertNotNull(admin);
	}
	@Test
	public void testNeedsAdminReturnsFalse() {
		assertFalse(DatabaseBootstrapper.needsAdminUser());
	}
	
	@Test
	public void testNeedsITDepartment() {
		assertFalse(DatabaseBootstrapper.needsItDepartment());
	}

}

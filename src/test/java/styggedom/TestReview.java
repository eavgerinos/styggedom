package styggedom;

import static org.junit.Assert.*;

import org.junit.Test;

import styggedom.entities.Review;
import styggedom.entities.User;

public class TestReview {

	@Test
	public void testCreatesNewReview() {
		int initialCount = Review.countAll();
		
		User user = User.userDao.fetchAll().get(0);
		Review review = new Review();
		review.setStars(2);
		review.setEmployee(user);
		review.setReviewer(user);
		review.setContent("asdasdasd");
		
		Review.reviewDao.persist(review);
		
		int afterSaveCount = Review.countAll();
		assertEquals(initialCount + 1, afterSaveCount);
		destroyReview(review);
	}
	
	@Test
	public void testFindsByEmployee() {
		User admin = User.userDao.findByUsername("admin");
		Review review = Review.reviewDao.findByEmployee(admin).get(0);
		
		assertEquals(admin.getUsername(), review.getEmployee().getUsername());
	}
	
	public void destroyReview(Review r) {
		Review.reviewDao.destroy(r);
	}
	
	@Test
	public void testFindByEmployeeAndContent() {
		User admin = User.userDao.findByUsername("admin");
		Review review = Review.reviewDao.findByEmployeeAndContent(admin, "Awesome Admin!");
		
		assertEquals(admin.getUsername(), review.getEmployee().getUsername());
		
	}
	
	@Test
	public void testGetContent() {
		User admin = User.userDao.findByUsername("admin");
		Review review = Review.reviewDao.findByEmployee(admin).get(0);
		
		assertEquals(review.getContent(), "Awesome Admin!");
		
	}
	
	@Test
	public void testGetStars() {
		User admin = User.userDao.findByUsername("admin");
		Review review = Review.reviewDao.findByEmployee(admin).get(0);
		
		assertEquals(review.getStars().intValue(), 4);
	}
	
	@Test
	public void testGetReviewer() {
		User admin = User.userDao.findByUsername("admin");
		Review review = Review.reviewDao.findByEmployeeAndContent(admin, "Awesome Admin!");
		
		assertEquals(admin.getUsername(), review.getReviewer().getUsername());
	}
	
	@Test
	public void testGetId() {
		User admin = User.userDao.findByUsername("admin");
		Review review = Review.reviewDao.findByEmployeeAndContent(admin, "Awesome Admin!");
		
		assertNotNull(review.getId());
	}

}

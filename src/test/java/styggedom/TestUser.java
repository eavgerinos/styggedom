package styggedom;

import static org.junit.Assert.*;

import org.junit.Test;

import styggedom.entities.Department;
import styggedom.entities.User;

public class TestUser {

	@Test
	public void testRetrievesExistingUserByUsername() {
		User user = User.userDao.findByUsername("admin");

		assertNotNull(user);
	}


	@Test
	public void testCreatesNewUser() {
		int initialCount = User.countAll();
		Department dept = Department.departmentDao.fetchAll().get(0);
		User user = new User();

		user.setIsAdmin(false);
		user.setFullName("A test user");
		user.setDepartment(dept);
		user.setIsCEO(false);
		user.setIsManager(false);
		user.setEmail("test@test.test");
		user.setPassword("test");
		user.setUsername("test");

		User.userDao.persist(user);
		
		int afterSaveCount = User.countAll();
		
		assertEquals(initialCount + 1, afterSaveCount);
		destroyTestUserIfExists();
	}
	
	@Test
	public void testUpdatesExistingUser() {
		User user = User.userDao.findByUsername("vladimir");

		User.userDao.update(user, user.getFullName(), user.getUsername(),
				user.getEmail(), "atestpassword", user.getIsCEO(),
				user.getIsAdmin(), user.getIsManager(), user.getDepartment());

		user = User.userDao.findByUsername("vladimir");

		assertEquals(user.getPassword(), "atestpassword");
	}
	
	@Test
	public void testDestroysExistingUser() {
		setTestUserIfNotExists();
		int initialCount = User.countAll();
		User user = User.userDao.findByUsername("test");
		
		User.userDao.destroy(user);
		
		int afterDestroyCount = User.countAll();
		assertEquals(initialCount - 1, afterDestroyCount);
	}
	
	@Test
	public void testSuccessfullAuthenticationReturnsLoggedInUser() {
		User user = User.userDao.authenticate("admin", "admin");
		
		assertNotNull(user);
		assertEquals(user.getIsAdmin(), true);
		
	}
	
	@Test
	public void testFailedAuthenticationReturnsNull() {
		assertNull(User.userDao.authenticate("woot", "woot"));
	}
	
	@Test
	public void testFetchAdminsReturnsAdmins() {
		User user = User.userDao.findAdmins().get(0);
		
		assertTrue(user.getIsAdmin());
	}
	
	@Test
	public void testFindByDepartmentReturnsUsersOfASpecificDept() {
		Department dept = Department.departmentDao.fetchAll().get(0);
		
		User user = User.userDao.findByDepartment(dept).get(0);
		
		if (user != null)
			assertEquals(user.getDepartment(), dept);
	}
	
	public static void setTestUserIfNotExists() {
		Department dept = Department.departmentDao.fetchAll().get(0);
		User user = User.userDao.findByUsername("test");
		if (user == null) {
			user = User.userDao.findByUsername("vladimir");
			user = new User();

			user.setIsAdmin(false);
			user.setFullName("A test user");
			user.setDepartment(dept);
			user.setIsCEO(false);
			user.setIsManager(false);
			user.setEmail("test@test.test");
			user.setPassword("test");
			user.setUsername("test");

			User.userDao.persist(user);
		}
	}
	
	public static void destroyTestUserIfExists() {
		User user = User.userDao.findByUsername("test");
		if(user != null) {
			User.userDao.destroy(user);
		}
	}

}

package styggedom;

import static org.junit.Assert.*;

import org.junit.Test;

import styggedom.entities.Department;
import styggedom.entities.User;

public class TestDepartment {

	@Test
	public void testRetrievesExistingDepartment() {
		Department dept = Department.departmentDao.fetchAll().get(0);

		assertNotNull(dept);
	}

	@Test
	public void testCreatesNewDepartment() {
		int initialCount = Department.countAll();
		Department dept = new Department();

		dept.setName("dept");
		Department.departmentDao.persist(dept);

		int afterSaveCount = Department.countAll();

		assertEquals(initialCount + 1, afterSaveCount);
		destroyTestDept(dept);
	}

	@Test
	public void itAssociatesWithUsers() {
		User admin = User.userDao.findByUsername("admin");
		Department dept = admin.getDepartment();

		assertEquals(dept.getName(), "IT");
		assertEquals(dept.getManager(), admin);
	}

	@Test
	public void itDoesNotDestroyDeptsWithUsers() {
		int initialCount = Department.countAll();
		User admin = User.userDao.findByUsername("admin");

		Department.departmentDao.destroy(admin.getDepartment());

		int afterDestroyCount = Department.countAll();

		assertEquals(initialCount, afterDestroyCount);

	}

	public static void destroyTestDept(Department dept) {
		Department.departmentDao.destroy(dept);
	}

}
